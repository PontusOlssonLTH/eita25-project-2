package server;

/**
 * This class represents a person in the person register and contains
 * information such as name, ssn-number, role and division. The division is
 * only relevant if the role of the person is either doctor or nurse. For
 * patients and government agencies, the division will be "N/A".
 */
public class Account {

	private String firstName;
	private String lastName;
	private String ssn;
	private String role;
	private String division;
	
	/**
	 * Constructor.
	 * @param firstName	The first name of the person
	 * @param lastName	The last name of the person
	 * @param ssn		The ssn-number of the person
	 * @param role		The role of the person
	 * @param division	The division associated with the person
	 */
	public Account(String firstName, String lastName, String ssn, String role, String division) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
		this.role = role;
		this.division = division;
	}
	/**
	 * @return	The full name of the person 
	 */
	public String getName() {
		return firstName + " " + lastName;
	}
	
	/**
	 * @return	The ssn-number of the person
	 */
	public String getSSN(){
		return ssn;
	}
	
	/**
	 * @return 	The role of the person
	 */
	public String getRole() {
		return role;
	}
	
	/**
	 * @return	The division associated with the person
	 */
	public String getDivision() {
		return division;
	}
	
}
