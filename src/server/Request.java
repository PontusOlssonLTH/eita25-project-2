package server;

/**
 * This class represents an access request and contain information about the
 * type of the request, the resource requested and possibly some other
 * information needed to perform the request.
 */
public class Request {
	
	private String action;
	private String resource;
	private String info;
	
	/**
	 * Constructor.
	 * @param action	The requested access type
	 * @param resource	The requested resource to access
	 */
	public Request(String action, String resource) {
		this.action = action;
		this.resource = resource;
	}
	
	/**
	 * Sets the attribute info to some extra information needed to perform the
	 * requested action. If the request is of type "WRITE", info will contain
	 * the text to be appended to the medical record. If the request if of type
	 * "CREATE", info will contain the ssn-numbers of the patient and nurse as
	 * well as the medical data.
	 * @param info	The extra information about the request.
	 */
	public void setInfo(String info) {
		this.info = info;
	}
	
	/**
	 * @return	The type of the request
	 */
	public String getAction() {
		return action;
	}
	
	/**
	 * @return 	The extra information associated with the request
	 */
	public String getInfo() {
		return info;
	}
	
	/**
	 * @return 	The requested resource to be accessed
	 */
	public String getResource() {
		return resource;
	}

}
