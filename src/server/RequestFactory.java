package server;

/**
 * This class is responsible for parsing string representations of requests
 * sent by the client and then generating Request object.
 */
public class RequestFactory {
	
	/**
	 * Parses a string representation of a request and returns a Request 
	 * object if possible. If the argument req is an invalid request or on an
	 * unrecognized format, null will be returned. 
	 * @param req	The string representation of the request
	 * @return		A Request if it was able to parse the input argument,
	 * 				otherwise null
	 */
	public Request createRequest(String req) {
		Request r = null;
		String[] s = req.split(" ");
		if (s.length == 1) {
			String action = s[0];
			if (action.equals("CREATE")) {
				r = new Request(action, null);
			}
		} else if (s.length == 2) {
			String action = s[0];
			String resource = s[1];
			if (validAction(action)) {
				r = new Request(action, resource);
			}
		}
		return r;
	}
	
	/**
	 * Checks if the requested action is valid
	 * @param action	The requested action
	 * @return			True if it is a valid command, otherwise false
	 */
	private boolean validAction(String action) {
		return (action.equals("READ") || action.equals("WRITE") || action.equals("DELETE"));
	}
}
