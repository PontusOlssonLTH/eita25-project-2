package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This class handles the dialog between the server and client by receiving
 * messages or commands from the client and responding to them.
 */
public class Dialog {
	private PrintWriter out;
	private BufferedReader in;
	private PEP pep;
	
	/**
	 * Constructor.
	 * @param in	The input stream
	 * @param out	The output stream 
	 * @param pep	The policy enforcement point
	 */
	public Dialog(BufferedReader in, PrintWriter out, PEP pep) {
		this.in = in;
		this.out = out;
		this.pep = pep;
	}
	
	/**
	 * This method handles the dialog with the client, or subject, by reading
	 * commands from the input stream and then replying with appropriate
	 * responses on the output stream.
	 * @param subject 	The ssn-number of the client with which the server is
	 * 					going to start a dialog with  
	 */
	public void startDialog(String subject) {
		 String recieved = null;
		 try {
	         while ((recieved = in.readLine()) != null) {
	         	RequestFactory rf = new RequestFactory();
	         	Request request = rf.createRequest(recieved);
	         	if (request == null) {
	         		String response = "ERROR: Ogiltig request.";
	         		out.println(response);
	         		out.flush();
	         	} else {
	         		if (request.getAction().equals("WRITE")) {
	         			out.println("Skriv texten du vill lägga till:");
	         			out.flush();
	         			String text;
	         			text = in.readLine();
	         			request.setInfo(text);
	         		} else if (request.getAction().equals("CREATE")) {
	         			out.println("Personnummer för patienten: ");
	         			String patientSSN = in.readLine();
	         			out.println("Personnummer för sjuksköterskan: ");
	         			String nurseSSN = in.readLine();
	         			out.println("Skriv den medicinska datan: ");
	         			String info = in.readLine();
	         			request.setInfo(patientSSN + ":" + nurseSSN + ":" + info);
	         		}
	         		String response = pep.handleRequest(subject, request);
	         		System.out.println(response);
	         		out.println(response);
	         		out.flush();
	         	}
	         }
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
	}
}
