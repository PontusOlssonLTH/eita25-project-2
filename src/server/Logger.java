package server;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * This class is responsible for logging all accesses to the medical records.
 * This is done by appending information about all access events to the end of
 * a text file.
 */
public class Logger {
	
	private FileWriter fw;
	private BufferedWriter bw;
	private PrintWriter pw;
	private Calendar cal;
	private SimpleDateFormat sdf;
	
	
	/**
	 * Constructor.
	 */
	public Logger() {
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * Logs an access event by appending information to a text file. 
	 * Information about who did what and when will be logged.
	 * @param subject	The subject who did something
	 * @param resource	The resource which the subject did something to
	 * @param command	What the subject did
	 */
	public void log(String subject, String resource, String command) {
		try {
			fw = new FileWriter("data/log.txt", true);
		    bw = new BufferedWriter(fw);
		    pw = new PrintWriter(bw);
		    cal = Calendar.getInstance(); 
		    String time = sdf.format(cal.getTime());
		    pw.println(time + ", " + subject + " " + command + " " + resource);
		    pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
