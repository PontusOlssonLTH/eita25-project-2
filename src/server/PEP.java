package server;

import java.util.HashMap;

/**
 * This class is responsible for enforcing the policies for the access control.
 * All request are handled by this class who will check with the Policy
 * Decision Point if the request should be permitted or denied. It will then
 * perform the request and log it.
 */
public class PEP {
	
	private FileHandler fh;
	private HashMap<String, Account> register;
	private HashMap<String, Record> medicalRecords;
	private PDP pdp;
	private Logger logger;
	
	/**
	 * Constructor.
	 */
	public PEP() {
		fh = new FileHandler();
		register = fh.initRegister();
		medicalRecords = fh.initRecords(register);
		pdp = new PDP(register, medicalRecords);
		logger = new Logger();
		Runtime r = Runtime.getRuntime();
        r.addShutdownHook(new Thread(){
        	public void run() {
               fh.writeRecordsToFile(medicalRecords);
            }
        });
	}
	
	/**
	 * Handles a new access request. The subject argument is the ssn-number of
	 * the person who made the request.
	 * @param subject	The subject who made the request
	 * @param request	The request to be handled
	 * @return			A textual response to send back to the client.
	 */
	public String handleRequest(String subject, Request request) {
		String response = null;
		String action = request.getAction();
		String resource = request.getResource();
		String info = request.getInfo();
		switch (action) {
			case "READ":
				response = readRecord(subject, resource); break;
			case "WRITE":
				response = writeRecord(subject, resource, info); break;
			case "CREATE":
				String[] infoP = info.split(":");
				if (infoP.length != 3)
					response = "INVALID INPUT";
				else {
					String patientSSN = infoP[0];
					String nurseSSN = infoP[1];
					String data = infoP[2];
					response = createRecord(subject, patientSSN, nurseSSN, data); 
				}
			break;
			case "DELETE":
				response = deleteRecord(subject, resource); break;
		}
		return response;
	}
	
	/**
	 * If the subject is authorized to read the resource, this method will
	 * return a string representation of the resource which the subject
	 * requested to read. It will then log the action.
	 * @param subject	The subject wanting to read the resource
	 * @param resource	The resource to be read
	 * @return			A string representation of the resource if subject was
	 * 					authorized to read it. Otherwise a string saying that
	 * 					it was an unauthorized command. 
	 */
	private String readRecord(String subject, String resource) {
		String response = "Unauthorized command";
		if (pdp.canRead(subject, resource)) {
			response = medicalRecords.get(resource).toString();
			logger.log(subject, resource, "READ");
		}
		return response;
	}
	
	/**
	 * If the subject if authorized to write to the specified resource, this
	 * method will add the text given by the argument text to the resource.
	 * It will then log the action.
	 * @param subject	The subject wanting to write to the resource
	 * @param resource	The resource to be written to
	 * @param text		The text to be added to the resource
	 * @return			A string saying that the text was written to the
	 * 					resource if the subject was authorized. Otherwise a 
	 * 					string saying that it was an unauthorized command.
	 */
	private String writeRecord(String subject, String resource, String text) {
		String response = "Unauthorized command";
		if (pdp.canWrite(subject, resource)) {
			try {
				medicalRecords.get(resource).write(text);
			} catch (Exception e) {
				e.printStackTrace();
			}
			response = "Record written to";
			logger.log(subject, resource, "WRITE");
		}
		return response; 
	}
	
	/**
	 * If the subject is authorized to create new medical records, this method
	 * will create a new medical record for the patient specified by the
	 * argument patient. It will then log the action.
	 * @param subject	The subject wanting to create a new medical record
	 * @param patient	The patient for the medical record
	 * @param nurse		The nurse associated with the medical record
	 * @param text		The medical data to be written in the medical record
	 * @return			If the subject was authorized, a string saying that a
	 * 					new record was successfully created along with a record
	 * 					ID for the record. Otherwise a string saying that it
	 * 					was an unauthorized command.
	 */
	private String createRecord(String subject, String patient, String nurse, String text) {
		String response = "Unauthorized command";
		if(pdp.canCreate(subject)){
			Account doc = register.get(subject);
			Account nur = register.get(nurse);
			Account pat = register.get(patient);
			Record record = new Record(pat, doc, nur, text);
			String recordID = record.getID();
			medicalRecords.put(recordID, record);
			response = "New record sucessfully created, recordID = " + recordID;
			logger.log(subject, recordID, "CREATE");
		}
		return response;
	}
	
	/**
	 * If the subject is authorized to delete the specified resource, this
	 * method will simply delete it from the medicalRecords hashMap. It will
	 * then log the action.
	 * @param subject	The subject wanting to delete the resource
	 * @param resource	The resource to be deleted
	 * @return			A string saying the record was successfully deleted if
	 * 					the subject was authorized. Otherwise a string saying
	 * 					that it was an unauthorized command.
	 */
	private String deleteRecord(String subject, String resource) {
		String response = "Unauthorized command";
		if(pdp.canDelete(subject, resource)){
			medicalRecords.remove(resource);
			response = "Record sucessfully deleted";
			logger.log(subject, resource, "DELETE");
		}
		return response;
	}
}
