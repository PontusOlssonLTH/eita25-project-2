package server;

import java.util.HashMap;

/**
 * This class is responsible for the decision making and determining if a
 * subject is authorized to perform the requested action. The class utilizes
 * information and attributes from the medical records and a register of all
 * individuals to make its decisions.  
 */
public class PDP {
	private HashMap<String, Account> register;
	private HashMap<String, Record> medicalRecords;
	
	/**
	 * Constructor.
	 * @param register			The person register
	 * @param medicalRecords	The medical records register.
	 */
	public PDP(HashMap<String, Account> register, HashMap<String, Record> medicalRecords) {
		this.register = register;
		this.medicalRecords = medicalRecords;
	}
	
	/**
	 * Checks if the subject is authorized to read the resource. The subject
	 * argument is the ssn-number of the person wanting to read and the 
	 * resource argument is the unique identifier of the resource to be read.
	 * @param subject	The subject who wants to read the resource
	 * @param resource	The resource to be read
	 * @return 			True if the subject is authorized to read the resource.
	 */
	public boolean canRead(String subject, String resource) {
		if (!medicalRecords.containsKey(resource)) return false;
		String role = register.get(subject).getRole();
		Record record = medicalRecords.get(resource);
		switch (role) {
			case "Patient":
				String patient = record.getPatient().getSSN();
				if (subject.equals(patient)) {
					return true;
				}
			break;
			case "Doctor":
				patient = record.getPatient().getSSN();
				String doctor = record.getDoctor().getSSN();
				String doctorDiv = register.get(subject).getDivision();
				String recordDiv = record.getDivision();
				if (subject.equals(patient) || subject.equals(doctor) || doctorDiv.equals(recordDiv)) {
					return true;
				}
			break;
			case "Nurse":
				patient = record.getPatient().getSSN();
				String nurse = record.getNurse().getSSN();
				String nurseDiv = register.get(subject).getDivision();
				recordDiv = record.getDivision();
				if (subject.equals(patient) || subject.equals(nurse) || nurseDiv.equals(recordDiv)) {
					return true;
				}
			break;
			case "GovAgency":
				return true;
		}
		return false;
	}
	
	/**
	 * Checks if the subject is authorized to write to the specified resource.
	 * The subject argument is the ssn-number of the person wanting to write
	 * and the resource argument is the unique identifier of the resource to
	 * be written to.
	 * @param subject	The subject who wants to write to the resource
	 * @param resource	The resource to be written to
	 * @return			True if the subject is authorized to write to the resource
	 */
	public boolean canWrite(String subject, String resource) {
		if (!medicalRecords.containsKey(resource)) return false;
		String role = register.get(subject).getRole();
		Record record = medicalRecords.get(resource);
		switch (role) {
			case "Doctor":
				String doctor = record.getDoctor().getSSN();
				if (subject.equals(doctor))
					return true;
				break;
			case "Nurse":
				String nurse = record.getNurse().getSSN();
				if (subject.equals(nurse)) 
					return true;
				break;
		}
		return false;
	}
	
	/**
	 * Checks if the subject is authorized to create a new medical record. The
	 * subject argument is the ssn-number of the person wanting to create a new
	 * medical record.
	 * @param subject	The subject who wants to create a record
	 * @return			True if the subject is authorized to create new records
	 */
	public boolean canCreate(String subject){
		String role = register.get(subject).getRole();
		return role.equals("Doctor");
	}
	
	/**
	 * Checks if the subject is authorized to delete the specified resource.
	 * The subject argument is the ssn-number of the person wanting to delete
	 * the resource and the resource argument is the unique identifier of the 
	 * resource to be deleted.
	 * @param subject	The subject who wants to delete the resource
	 * @param resource	The resource to be deleted
	 * @return			True if the subject is authorized to delete the resource
	 */
	public boolean canDelete(String subject, String resource){
		if (!medicalRecords.containsKey(resource)) return false;
		String role = register.get(subject).getRole();
		if(role.equals("GovAgency")){
			return true;
		}
		return false;
	}
}
