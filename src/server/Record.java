package server;

/**
 * This class represents a medical record and contains all information
 * associated with the medical records.
 */
public class Record {
	private int id;
	private String division;
	private Account patient;
	private Account doctor;
	private Account nurse;
	private String info;
	
	/**
	 * Constructor for Record with predefined ID. 
	 * Used when reading medical records from file.
	 * @param id		The ID of the record
	 * @param patient	The patient associated with the record
	 * @param doctor	The doctor associated with the record
	 * @param nurse		The nurse associated with the record
	 * @param info		The medical data
	 */
	public Record(int id, Account patient, Account doctor, Account nurse, String info) {
		this.id = id;
		division = doctor.getDivision();
		this.patient = patient;
		this.doctor = doctor;
		this.nurse = nurse;
		this.info = info;
	}
	
	/**
	 * Constructor for Record without predefined ID.
	 * Used when a doctor creates a new record.
	 * @param patient	The patient associated with the record
	 * @param doctor	The doctor associated with the record
	 * @param nurse		The nurse associated with the record
	 * @param info		The medical data
	 */
	public Record(Account patient, Account doctor, Account nurse, String info) {
		this(FileHandler.largestID + 1, patient, doctor, nurse, info);
	}
	
	/**
	 * @return 	The ID of the record
	 */
	public String getID() {
		return Integer.toString(id);
	}
	
	/**
	 * @return 	The division associated with the record
	 */
	public String getDivision() {
		return division;
	}

	/**
	 * @return 	The patient associated with the record
	 */
	public Account getPatient() {
		return patient;
	}
	
	/**
	 * @return 	The doctor associated with the record
	 */
	public Account getDoctor() {
		return doctor;
	}
	
	/**
	 * @return	The nurse associated with the record
	 */
	public Account getNurse() {
		return nurse;
	}
	
	/**
	 * @param s				The string to be appended to the medical data
	 * @throws Exception	if s contains either ":" or "\n"
	 */
	public void write(String s) throws Exception {
		if(s.contains(":") || s.contains("\n"))
			throw new Exception("INVALID INFO WRITTEN TO RECORD");
		info += " >" + s;
	}

	/**
	 * @return 	A string representation of the medical record on the format to
	 * 			be stored in a text file with all other medical records.
	 */
	public String printFormat() {
		return id+":"+division+":"+patient.getSSN()+":"+doctor.getSSN()+":"+nurse.getSSN()+":"+info;
	}
	
	public String toString() {
		return "ID: " + id + "\n" + 
			   "Division: " + division + "\n" +
			   "Patient: " + patient.getName() + "\n" +
			   "Doctor: " + doctor.getName() + "\n" +
			   "Nurse: " + nurse.getName() + "\n" +
			   "Information: " + info;
	}
	
}
