package server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * This class is responsible for reading and writing to files. The person
 * register file along with the medical records file will be read when the
 * server program is started, and the medical records file will be written to
 * when the server program is terminated.  
 */
public class FileHandler {
	
	public static int largestID;
	
	/**
	 * Initializes the person register by reading the file register.txt and
	 * storing every person in a HashMap where ssn-numbers maps to Account
	 * objects representing the persons.
	 * @return	A HashMap containing all persons
	 */
	public HashMap<String, Account> initRegister() {
		HashMap<String, Account> map = new HashMap<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("data/register.txt"));
			String line = br.readLine();
			while (line != null) {
				String field[] = line.split(":");
				String firstName = field[0];
				String lastName = field[1];
				String ssn = field[2];
				String role = field[3];
				String division = field[4];
				Account a = new Account(firstName, lastName, ssn, role, division);
				map.put(ssn, a);
				line = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * Initializes the medical records register by reading the file
	 * medicalrecords.txt and storing every medical record in a HashMap where
	 * the record ID maps to the Record object representing the medical record.
	 * This method utilizes the person register to add certain information to
	 * the medical record.
	 * @param reg	The register of all persons
	 * @return		A HashMap containing all medical records
	 */
	public HashMap<String, Record> initRecords(HashMap<String, Account> reg) {
		HashMap<String, Record> map = new HashMap<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("data/medicalrecords.txt"));
			String line = br.readLine();
			while(line != null){
				String[] sa = line.split(":");
				String id = sa[0];
				int recordID = Integer.parseInt(id);
				largestID = (recordID > largestID) ? recordID: largestID; 
				Account patient = reg.get(sa[2]);
				Account doctor = reg.get(sa[3]);
				Account nurse = reg.get(sa[4]);
				String info = sa[5];
				map.put(id, new Record(recordID, patient, doctor, nurse, info));
				line = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * Writes all medical records in the HashMap given by the argument to a
	 * text file. This method is called by a shutdown hook to make sure no
	 * medical records are lost if the server program is terminated.
	 * @param mr	HashMap with all medical records
	 */
	public void writeRecordsToFile(HashMap<String, Record> mr) {
		try {
			PrintWriter pw = new PrintWriter("data/MedicalRecords.txt");
			for (Record record : mr.values()) {
			    pw.println(record.printFormat());
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
