package client;

import java.io.*;
import javax.net.ssl.*;
import java.security.KeyStore;
import java.util.Arrays;

public class Client {

    public static void main(String[] args) throws Exception {
        String host = null;
        int port = -1;
        if (args.length < 2) {
            System.out.println("USAGE: java client host port");
            System.exit(-1);
        }
        try {
            host = args[0];
            port = Integer.parseInt(args[1]);
        } catch (IllegalArgumentException e) {
            System.out.println("USAGE: java client host port");
            System.exit(-1);
        }

        try {
            SSLSocketFactory factory = null;
            try {
            	Console console = System.console();
            	System.out.print("ssn-number: ");
            	String ssn = console.readLine();
            	System.out.println("Password: ");
            	char[] password = console.readPassword();
                KeyStore ks = KeyStore.getInstance("JKS");
                KeyStore ts = KeyStore.getInstance("JKS");
                KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
                TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
                SSLContext ctx = SSLContext.getInstance("TLS");
                ks.load(new FileInputStream("Certificates/" + ssn + "/clientkeystore"), password);
				ts.load(new FileInputStream("Certificates/" + ssn + "/clienttruststore"), password);
				kmf.init(ks, password);
				tmf.init(ts);
				ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
                factory = ctx.getSocketFactory();
                Arrays.fill(password, '0');
            } catch (Exception e) {
                throw new IOException(e.getMessage());
            }
            SSLSocket socket = (SSLSocket)factory.createSocket(host, port);
            socket.startHandshake();

            BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
            System.out.println("You are now connected.\n");
            String msg;
			for (;;) {
                System.out.print(">");
                msg = read.readLine();
                if (msg.equalsIgnoreCase("quit")) {
				    break;
				}
                out.println(msg);
                out.flush();
                do {                	
                	String response = in.readLine();
                	System.out.println(response);                	
                } while(in.ready());
            }
            in.close();
			out.close();
			read.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

